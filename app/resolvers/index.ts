import { UserSchema, PostSchema } from '../models';

type userArguments = {
    id: String,
}

const resolvers = {
    hello: ()=> 'first result returned',
    user: (args: userArguments): Promise<any> => {
        return UserSchema.findById(args.id);
    },
    post: (args: userArguments, a: any, b: any): Promise<any> => {
        console.log('post args', 123, args, a, b)
        return PostSchema.findOne({userID: args.id})
    }
};

export default resolvers;
