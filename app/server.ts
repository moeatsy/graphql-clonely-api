import express from 'express';
import graphqlHTTP from 'express-graphql';
import typeDefs from './typeDefs';
import resolvers from './resolvers';
import mongoose from 'mongoose';

const path = require('path');

const app: express.Application = express();
const port: number = 3000;

mongoose.connect('mongodb://admin:zav15zav@ds159036.mlab.com:59036/clonely', {useNewUrlParser: true})
    .then(()=> console.log('Mongoose connected'),
          () => console.log('Mongoose connection error'));
mongoose.set('useFindAndModify', false);

app.use('/dev',express.static(path.join(__dirname, '../public/')));

app.use(
    '/graphql',
    graphqlHTTP({
        schema: typeDefs,
        graphiql: true,
        rootValue: resolvers,
    }),
);

app.get('/', (req: express.Request, res: express.Response) =>
    res.send('Ok ci/cd still working'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
