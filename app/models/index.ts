const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Song = new Schema({
    id: {
        type: String,
        required: true,
    },
    src: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
    track: {
        type: String,
        required: true,
    },
    img: {
        type: String,
        required: true,
    },
    duration: {
        type: Number,
        required: true,
    }
});

const Playlist = new Schema({
    caption: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
    },
    authorUrl: {
        type: String,
        required: true,
    },
    cover: {
        type: String,
        required: true,
    },
    songs: [Song]
});

const UserPlaylist = new Schema({
    lastIndex: {
        type: Number,
        required: true
    },
    songs: [Song]
});

const User = new Schema({
    auth_username : {
        type: String,
        required: true,
        lowercase: true,
    },
    auth_password : {
        type: String,
        required: true,
    },
    firstname : {
        type: String,
        required: true,
        lowercase: true,
    },
    lastname : {
        type: String,
        required: true,
        lowercase: true,
    },
    city : {
        type: String,
        required: true,
    },
    alias : {
        type: String,
        required: true,
        lowercase: true,
    },
    birthday : {
        type: String,
        required: true,
    },
    status : {
        type: String,
        required: true,
    },
    avatar : {
        type: String,
        required: true,
    },
    icon : {
        type: String,
        required: true,
    },
    userPlaylist : UserPlaylist,
    playlists : [Schema.Types.ObjectId],
    lastIndex : {
        type: Number,
        required: true,
    }
});

const Author = new Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    icon: {
        type: String,
        required: true,
    }
});

const Chat = new Schema({
    participants: [Author],
    lastMessage: {
        type: String,
        required: true,
    },
    lastMessageUnread: {
        type: Boolean,
        required: true
    },
    author: {
        type: Schema.Types.ObjectId,
        required: true
    }
});

const Message = new Schema({
    chatID: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    author: Author,
    message: {
        type: String,
        required: true,
    },
    unread: {
        type: Boolean,
        required: true
    }
});

const Post = new Schema({
    userID: Schema.ObjectId,
    privateFeed: {
        type: Boolean,
        required: true,
    },
    author: Author,
    pinned: {
        type: Boolean,
        required: true,
    },
    disableComments: {
        type: Boolean,
        required: true,
    },
    textBody: String,
    timestamp: Number,
    music: [],
    likes: [{
        userID: Schema.ObjectId
    }],
    commentsDisabled: Boolean
});

const all = {
    //   UserSchema: mongoose.model('User', User, 'mongoose_users'),
    ChatSchema: mongoose.model('Chat', Chat, 'mongoose_chats'),
    MessageSchema: mongoose.model('Message', Message, 'mongoose_messages'),
    PlaylistSchema: mongoose.model('Playlist', Playlist, 'mongoose_playlists'),
    //PostSchema: mongoose.model('Post', Post, 'mongoose_posts')
};

export const PostSchema =  mongoose.model('Post', Post, 'mongoose_posts')
export const UserSchema = mongoose.model('User', User, 'mongoose_users');
