import { buildSchema } from "graphql";

const schema = buildSchema(`
  type Query {
    hello: String
    user(id: ID!): User
    post(id: ID!): Post
  }
  type User {
    id: ID
    firstname: String!
    lastname: String!
    city: String
    status: String
    birthday: String
    avatar: String
    icon: String
    posts: [Post]
  }
  type Post {
    id: ID
    textBody: String
    privateFeed: Boolean
  }
`);

export default schema;
